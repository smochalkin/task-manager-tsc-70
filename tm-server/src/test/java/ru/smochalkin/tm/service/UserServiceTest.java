package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.configuration.ContextConfiguration;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;

public class UserServiceTest {

    @NotNull
    private IUserService userService;

    private int userCount;

    @Before
    public void init() {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        userService = context.getBean(IUserService.class);
        userService.create("test1", "1", "@");
        userService.create("test2", "2", "@");
        userCount = userService.getCount();
    }

    @After
    public void end() {
        userService.removeByLogin("test1");
        userService.removeByLogin("test2");
        userService.removeByLogin("test3");
    }

    @Test
    public void createTest() {
        Assert.assertEquals(userCount, userService.getCount());
        userService.create("test3", "3", "@");
        Assert.assertEquals(userCount + 1, userService.getCount());
    }

    @Test
    public void createWithEmailTest() {
        Assert.assertEquals(userCount, userService.getCount());
        userService.create("test3", "3", "t@t");
        Assert.assertEquals(userCount + 1, userService.getCount());
    }


    @Test
    public void findByLoginTest() {
        Assert.assertEquals("test1", userService.findByLogin("test1").getLogin());
    }

    @Test
    public void removeByLoginTest() {
        Assert.assertEquals(userCount, userService.getCount());
        userService.removeByLogin("test1");
        Assert.assertEquals(userCount - 1, userService.getCount());
    }

    @Test
    public void setPasswordTest() {
        @NotNull final UserDto userDto = userService.findByLogin("test1");
        @NotNull final String hash = userDto.getPasswordHash();
        userService.setPassword(userDto.getId(), "1000");
        Assert.assertNotEquals(hash, userService.findByLogin("test1").getPasswordHash());
    }

    @Test
    public void updateByIdTest() {
        @NotNull final UserDto userDto = userService.findByLogin("test1");
        @NotNull final String userId = userDto.getId();
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(userId, newFirstName, newLastName, newMiddleName);
        @NotNull final UserDto updatedUserDto = userService.findByLogin("test1");
        Assert.assertEquals(userDto.getId(), updatedUserDto.getId());
        Assert.assertEquals("new first name", updatedUserDto.getFirstName());
        Assert.assertEquals("new middle name", updatedUserDto.getMiddleName());
        Assert.assertEquals("new last name", updatedUserDto.getLastName());
    }

    @Test
    public void isLoginTest() {
        Assert.assertThrows(EmptyLoginException.class,
                () -> userService.isLogin("")
        );
        Assert.assertTrue(userService.isLogin("test1"));
    }

    @Test
    public void lockUserByLoginTest() {
        Assert.assertFalse(userService.findByLogin("test1").isLock());
        userService.lockUserByLogin("test1");
        Assert.assertTrue(userService.findByLogin("test1").isLock());
    }

    @Test
    public void unlockUserByLoginTest() {
        userService.lockUserByLogin("test1");
        Assert.assertTrue(userService.findByLogin("test1").isLock());
        userService.unlockUserByLogin("test1");
        Assert.assertFalse(userService.findByLogin("test1").isLock());
    }

}

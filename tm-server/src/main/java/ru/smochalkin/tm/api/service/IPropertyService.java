package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    String getSignSecret();

    @NotNull
    Integer getSignIteration();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getUseSecondLevelCache();

    @NotNull
    String getUseQueryCache();

    @NotNull
    String getUseMinimalPuts();

    @NotNull
    String getCacheRegionPrefix();

    @NotNull
    String getCacheProviderConfig();

    @NotNull
    String getCacheRegionFactory();

}

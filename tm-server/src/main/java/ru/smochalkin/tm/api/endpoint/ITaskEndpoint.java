package ru.smochalkin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.dto.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {
    @WebMethod
    @SneakyThrows
    Result createTask(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    @SneakyThrows
    Result changeTaskStatusById(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "id") @NotNull String id,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    Result changeTaskStatusByIndex(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "index") @NotNull Integer index,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    Result changeTaskStatusByName(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    List<TaskDto> findTaskAllSorted(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "sort") @NotNull String strSort
    );

    @WebMethod
    @SneakyThrows
    List<TaskDto> findTaskAll(
            @WebParam(name = "session") @NotNull SessionDto sessionDto
    );

    @WebMethod
    @SneakyThrows
    TaskDto findTaskById(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    @SneakyThrows
    TaskDto findTaskByName(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "name") @NotNull String name
    );

    @WebMethod
    @SneakyThrows
    TaskDto findTaskByIndex(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "index") @NotNull Integer index
    );

    @WebMethod
    @SneakyThrows
    Result removeTaskById(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    @SneakyThrows
    Result removeTaskByName(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "name") @NotNull String name
    );

    @WebMethod
    @SneakyThrows
    Result removeTaskByIndex(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "index") @NotNull Integer index
    );

    @WebMethod
    @SneakyThrows
    Result clearTasks(
            @WebParam(name = "session") @NotNull SessionDto sessionDto
    );

    @WebMethod
    @SneakyThrows
    Result updateTaskById(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "id") @NotNull String id,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    @SneakyThrows
    Result updateTaskByIndex(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "index") @NotNull Integer index,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    @SneakyThrows
    List<TaskDto> findTasksByProjectId(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "projectId") @NotNull String projectId
    );

    @WebMethod
    @SneakyThrows
    Result bindTaskByProjectId(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId") @NotNull String taskId
    );

    @WebMethod
    @SneakyThrows
    Result unbindTaskByProjectId(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId") @NotNull String taskId
    );
}

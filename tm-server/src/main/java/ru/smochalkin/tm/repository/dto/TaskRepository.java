package ru.smochalkin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.smochalkin.tm.dto.TaskDto;

import java.util.List;

public interface TaskRepository  extends JpaRepository<TaskDto, String> {

    void deleteByUserId(@NotNull String userId);

    void deleteByProjectId(@NotNull String projectId);

    @NotNull
    List<TaskDto> findAllByUserId(@NotNull String userId);

    @Nullable
    TaskDto findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    TaskDto findFirstByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    long countByUserId(@NotNull String userId);

    @Modifying
    @Query("UPDATE TaskDto e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :id")
    void bindTaskById(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId,
            @Param("id") @NotNull String taskId
    );
    @Modifying
    @Query("UPDATE TaskDto e SET e.projectId = NULL WHERE e.userId = :userId AND e.projectId = :projectId AND e.id = :id")
    void unbindTaskById(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId,
            @Param("id") @NotNull String taskId
    );

    @NotNull
    List<TaskDto> findAllByUserIdAndProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

}

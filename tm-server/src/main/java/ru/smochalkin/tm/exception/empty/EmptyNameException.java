package ru.smochalkin.tm.exception.empty;

import ru.smochalkin.tm.exception.AbstractException;

public final class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}

package ru.smochalkin.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.comparator.*;
import ru.smochalkin.tm.exception.system.SortNotFoundException;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.INSTANCE),
    STATUS("Sort by status", ComparatorByStatus.INSTANCE),
    CREATED("Sort by created", ComparatorByCreated.INSTANCE),
    START_DATE("Sort by start date", ComparatorByStartDate.INSTANCE),
    END_DATE("Sort by end date", ComparatorByEndDate.INSTANCE);

    private final String displayName;

    @NotNull
    private final Comparator comparator;

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public static Sort getSort(String value){
        try {
            return Sort.valueOf(value);
        } catch (IllegalArgumentException e) {
            throw new SortNotFoundException();
        }
    }

}

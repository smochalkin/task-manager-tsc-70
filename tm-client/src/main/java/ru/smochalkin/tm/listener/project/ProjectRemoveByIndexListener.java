package ru.smochalkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIndexListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove project by index.";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIndexListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter project index: ");
        @NotNull final Integer index = TerminalUtil.nextInt();
        @NotNull final Result result = projectEndpoint.removeProjectByIndex(sessionService.getSession(), index);
        printResult(result);
    }

}

package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.exception.EmptyIdException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.repository.IProjectRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepository repository;

    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    public void addAll(final Collection<Project> collection) {
        if (collection == null) return;
        for (Project item : collection) {
            save(item);
        }
    }

    @Override
    @SneakyThrows
    public void addAll(final String userId, @Nullable final Collection<Project> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (Project item : collection) {
            item.setUserId(userId);
            save(item);
        }
    }

    @Override
    public Project save(final Project entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project save(final String userId, @Nullable final Project entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        @Nullable final Project entityResult = save(entity);
        return entityResult;
    }

    @Override
    public void create() {
        repository.save(new Project("project"));
    }

    @Override
    public void create(final String userId) {
        save(userId, new Project("project"));
    }

    @Override
    public Project findById(final String id) {
        final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).get();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }

    @Override
    public void removeById(final String id) {
        final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public void remove(final Project entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

}

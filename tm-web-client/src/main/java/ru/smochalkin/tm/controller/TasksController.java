package ru.smochalkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.model.CustomUser;
import ru.smochalkin.tm.model.Project;

import java.util.List;

@Controller
public class TasksController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/tasks")
    public ModelAndView index(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user
    ) {
        return new ModelAndView("task-list", "tasks", taskService.findAll(user.getUserId()));
    }

    @ModelAttribute("projects")
    public List<Project> getProjects(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user
    ) {
        return projectService.findAll(user.getUserId());
    }

    @ModelAttribute("viewName")
    public String getViewName() {
        return "Tasks";
    }

}
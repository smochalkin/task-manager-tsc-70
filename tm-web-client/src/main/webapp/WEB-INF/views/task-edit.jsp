<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="../include/_header.jsp"/>
<form:form action="/task/edit/" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id"/>
    <table>
        <tr>
            <td>Name:</td>
            <td>Status:</td>
            <td>Project:</td>
            <td>Finish date:</td>
            <td>Description:</td>
        </tr>
        <tr>
            <td>
                <form:input type="text" path="name"/>
            </td>
            <td>
                <form:select path="status">
                    <form:option value="${null}" label="---"></form:option>
                    <form:options items="${statuses}" itemLabel="displayName"></form:options>
                </form:select>
            </td>
            <td>
                <form:select path="projectId">
                    <form:option value="${null}" label="---"></form:option>
                    <form:options items="${projects}" itemLabel="name" itemValue="id"></form:options>
                </form:select>
            </td>
            <td>
                <form:input type="date" path="endDate"/>
            </td>
            <td>
                <form:input type="text" path="description"/>
            </td>
        </tr>
    </table>
    <button type="submit">Submit</button>
</form:form>
<jsp:include page="../include/_footer.jsp"/>
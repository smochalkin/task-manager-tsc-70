<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="../include/_header.jsp"/>
<form:form action="/project/edit/" method="POST" modelAttribute="project">
    <form:input type="hidden" path="id"/>
    <table>
        <tr>
            <td>Name:</td>
            <td>Status:</td>
            <th>Finish date:</th>
            <td>Description:</td>
        </tr>
        <tr>
            <td>
                <form:input type="text" path="name"/>
            </td>
            <td>
                <form:select path="status">
                    <form:option value="${null}" label="---"></form:option>
                    <form:options items="${statuses}" itemLabel="displayName"></form:options>
                </form:select>
            </td>
            <td>
                <form:input type="date" path="endDate"/>
            </td>
            <td>
                <form:input type="text" path="description"/>
            </td>
        </tr>
    </table>
    <button type="submit">Submit</button>
</form:form>
<jsp:include page="../include/_footer.jsp"/>